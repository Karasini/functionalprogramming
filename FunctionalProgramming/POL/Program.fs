﻿// Learn more about F# at http://fsharp.org

open System

let rec readWords currentWordNumber maxWordNumber words =
    if currentWordNumber < maxWordNumber then
        let word = Console.ReadLine()
        readWords (currentWordNumber+1) maxWordNumber (words @ [word.[0..word.Length/2 - 1]])
    else
        Console.WriteLine(words |> String.concat Environment.NewLine)

[<EntryPoint>]
let main argv =
    let number = Console.ReadLine()
    let intNumber = Int32.Parse(number)
    readWords 0 intNumber []
    0