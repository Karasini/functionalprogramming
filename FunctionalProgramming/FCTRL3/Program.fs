﻿// Learn more about F# at http://fsharp.org

open System

let rec factorial n =
    match n with
    | _ when n < 2 -> 1
    | _ -> n * (factorial (n-1))

let getNumberDescription number:string =
    let stringNumber = number.ToString()
    let length = stringNumber.Length
    match length with 
    | _ when length <= 1 -> String.Format("0 {0}", stringNumber)
    | _ -> String.Format("{0} {1}", stringNumber.[stringNumber.Length - 2], stringNumber.[stringNumber.Length - 1])

let showNumbers (numbers : int list) =
    let returnValue = List.fold(fun init number -> init + String.Format("{0}{1}", (getNumberDescription number), Environment.NewLine)) "" numbers
    Console.WriteLine(returnValue)

let rec readWords currentWordNumber maxWordNumber words =
    if currentWordNumber < maxWordNumber then
        let number = Int32.Parse(Console.ReadLine())
        readWords (currentWordNumber+1) maxWordNumber (words @ [factorial number])
    else
        showNumbers words

[<EntryPoint>]
let main argv =
    let number = Console.ReadLine()
    let intNumber = Int32.Parse(number)
    readWords 0 intNumber []
    0