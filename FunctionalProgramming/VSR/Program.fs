﻿open System

let calculate x1 x2:int =
    let x1 = Int32.Parse(x1)
    let x2 = Int32.Parse(x2)
    (2 * x1 * x2) / (x1 + x2)

let rec readNumbers current max numbers =
    if current < max then
        let newNumbers = Console.ReadLine().Split(' ')
        readNumbers (current+1) max (numbers @ [newNumbers])
    else
        numbers |> List.map (fun x-> printfn "%i" (calculate x.[0] x.[1]))

[<EntryPoint>]
let main argv =
    let intNumber = Int32.Parse(Console.ReadLine())
    readNumbers 0 intNumber []
    0