﻿// Learn more about F# at http://fsharp.org

open System
open System.Linq

let sumNumberWithReverse number =
    let reverse = new String(number.ToString().Reverse().ToArray())
    number + Int32.Parse(reverse)

let rec checkPalindromeOrSum actualNumber steps =
    let numberString = actualNumber.ToString()

    if numberString.SequenceEqual(numberString.Reverse()) then
        String.Format("{0} {1}", actualNumber, steps)
    else
        checkPalindromeOrSum (sumNumberWithReverse actualNumber) (steps+1)

let rec readNumbers current max numbers =
    if current < max then
        let newNumber = Int32.Parse(Console.ReadLine())
        readNumbers (current+1) max (numbers @ [checkPalindromeOrSum newNumber 0])
    else
        numbers |> List.map (fun x-> printfn "%s" x)

[<EntryPoint>]
let main argv =
    let intNumber = Int32.Parse(Console.ReadLine())
    readNumbers 0 intNumber []
    0 // return an integer exit code
