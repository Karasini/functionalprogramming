﻿// Learn more about F# at http://fsharp.org

open System

[<EntryPoint>]
let main argv =
    let x = 2 in printfn "%d+%d=%d" x x (x+x)
    let sqr v = v * v
    printfn "%f" (sqr 2.0)
    0 // return an integer exit code
