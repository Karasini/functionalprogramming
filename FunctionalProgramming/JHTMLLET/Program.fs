﻿// Learn more about F# at http://fsharp.org

open System
open System.Text.RegularExpressions

let toUpper word =
    Regex.Replace(word, "(?<=\<)(.*?)(?=\>)", MatchEvaluator(fun m -> m.Value.ToUpper()))

let rec readWords words =
    let word = Console.ReadLine()
    match word with
    | _ when String.IsNullOrEmpty(word) = false -> readWords (words @ [toUpper word])
    | _ -> printf "%s" (words |> String.concat Environment.NewLine)

[<EntryPoint>]
let main argv =
    readWords []
    0 // return an integer exit code
