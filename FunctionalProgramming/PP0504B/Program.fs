﻿// Learn more about F# at http://fsharp.org

open System
open System.Text

let mergeStrings (text1 : string) (text2 : string) =
    let builder = new StringBuilder()
    let maxIndex = (min text1.Length text2.Length) - 1
    for i in 0 .. maxIndex do
        builder.Append(text1.[i])
        builder.Append(text2.[i])

    builder.ToString()

let rec readNumbers current max numbers =
    if current < max then
        let newNumbers = Console.ReadLine().Split(' ')
        readNumbers (current+1) max (numbers @ [mergeStrings newNumbers.[0] newNumbers.[1]])
    else
        numbers |> List.map (fun x-> printfn "%s" x)

[<EntryPoint>]
let main argv =
    let intNumber = Int32.Parse(Console.ReadLine())
    readNumbers 0 intNumber []
    0 // return an integer exit code
